﻿#pragma once

#include <memory>

template < typename T, class Allocator = std::allocator< T > >
class narytree
{
public:
	typedef T value_type;
	typedef Allocator allocator_type;
	typedef typename Allocator::reference reference;
	typedef typename Allocator::const_reference const_reference;
	typedef typename Allocator::size_type size_type;
	typedef typename Allocator::difference_type difference_type;
	typedef typename Allocator::pointer pointer;
	typedef typename Allocator::const_pointer const_pointer;

private:
	// リンク
	struct link
	{
		link * prev;
		link * next;
		link * parent;
	};
	
	// ノードベース
	struct node_base
		: public link
	{
		size_type des_size;	// 子孫の数

		link ter;			// 終端
	};
	
	// ノード
	struct node
		: public node_base
	{
		value_type data;
	};

private:
	// アロケータを node にバインドする
	typedef typename Allocator::template rebind< node >::other alloc_type;

private:
	node * create_node( const T & t )
	{
		node * n = _n_alloc.allocate( 1 );
		
		init_node( n );
		
		try
		{
			_alloc.construct( &( n->data ), t );
		}
		catch ( ... )
		{
			_n_alloc.deallocate( n, 1 );
		}

		return n;
	}
	
	void delete_node( node * n )
	{
		_alloc.destroy( &( n->data ) );

		_n_alloc.deallocate( n, 1 );
	}
	
	void init_node( node_base * n )
	{
		n->des_size = 0;
		
		n->prev = n->next = n->parent = 0;
		
		n->ter.prev = n->ter.next = &( n->ter );
		
		n->ter.parent = n;
	}
	
	void delete_node_des( node * n );

private:
	node_base _root;		// ルート・ノード
	alloc_type _n_alloc;	// ノードアロケータ
	Allocator _alloc;		// アロケータ

private:
	// イテレータベース
	struct iterator_base
	{
		friend narytree;
		typedef ptrdiff_t difference_type;
		typedef T value_type;
		iterator_base() : _n( 0 ) {}
		iterator_base( link * n ) : _n( (node_base * )n ) {}
		bool operator==( const iterator_base & rhs ) const { return _n == rhs._n; }
		bool operator!=( const iterator_base & rhs ) const { return _n != rhs._n; }
		bool operator<( const iterator_base & rhs ) const { return _n < rhs._n; }
		size_type des_size() const { return _n->des_size; }
	protected:
		node_base * _n;
	};
	
public:
	// コンストイテレータ
	struct const_iterator
		: public iterator_base
	{
		typedef const T * pointer;
		typedef const T & reference;
		const_iterator( link * n ) : iterator_base( n ) {}
		const_iterator() : iterator_base( 0 ) {}
		reference operator*() const { return ( (node *)_n )->data; }
		pointer operator->() const { return &( operator*() ); }
		// 前置
		const_iterator & operator++()
		{
			this->_n = (node_base *)_n->next;
			
			return *this;
		}
		// 後置
		const_iterator operator++( int )
		{
			const_iterator t = *this;
			
			this->_n = (node_base *)_n->next;
			
			return t;
		}
		const_iterator & operator--()
		{
			this->_n = (node_base *)_n->prev;
			
			return *this;
		}
		const_iterator operator--( int )
		{
			const_iterator t = *this;
			
			this->_n = (node_base*)_n->prev;
			
			return t;
		}
		const_iterator parent() const { return const_iterator( _n->parent ); }
		const_iterator begin() const { return const_iterator( _n->ter.next ); }
		const_iterator end() const { return const_iterator( &( _n->ter ) ); }
		pointer data() const { return &( operator*() ); }
	};

	// イテレータ
	struct iterator
		: public const_iterator
	{
		typedef T * pointer;
		typedef T & reference;
		iterator( link * n ) : const_iterator( n ) {}
		iterator() : const_iterator( 0 ) {}
		reference operator*() const { return ( (node *)_n )->data; }
		pointer operator->() const { return &( operator*() ); }
		// 前置
		iterator & operator++()
		{
			_n = (node_base *)_n->next;
			
			return *this;
		}
		// 後置
		iterator operator++( int )
		{
			iterator t = *this;
			
			_n = (node_base *)_n->next;
			
			return t;
		}
		iterator & operator--()
		{
			_n = ( node_base * )_n->prev;
			
			return *this;
		}
		iterator operator--( int )
		{
			iterator t = *this;
			
			_n = ( node_base *)_n->prev;
			
			return t;
		}
		iterator parent() const { return iterator( _n->parent ); }
		iterator begin() const { return iterator( _n->ter.next ); }
		iterator end() const { return iterator( &( _n->ter ) ); }
		pointer data() const { return &( operator*() ); }
	};

private:
	void copy_node(
		const_iterator b, 
		const_iterator e, 
		iterator to );

public:
	narytree()
	{
		init_node( &_root );
	}
	
	narytree( const narytree< T > & t )
	{
		init_node( &_root );
		
		copy_node( t.begin(), t.end(), end() );
	}

	~narytree()
	{
		clear();
	}

	iterator insert( const iterator & it, const T & t );
	iterator erase( const iterator & it );
	void clear();

	iterator       root() { return iterator( &_root ); }
	const_iterator root() const { return const_iterator( &_root ); }
	iterator       begin() { return iterator( _root.ter.next ); }
	const_iterator begin() const { return const_iterator( _root.ter.next ); }
	iterator       end() { return iterator( &_root.ter ); }
	const_iterator end() const { return const_iterator( const_cast< link * >( &_root.ter ) ); }
	size_type size() const { return _root.des_size; }
	bool empty() const { return size() == 0; }
};

////////////////////////////////////////////////////////////////////////////////
////
//// private methods
////

template < class T, class Allocator >
void narytree< T, Allocator >::delete_node_des( node * n )
{
	for ( node * m = (node *)n->ter.next; m != (node *)&n->ter; )
	{
		node * u = (node *)m->next;
		
		delete_node_des( m );
		
		m = u;
	}
	
	delete_node( n );
}

template < class T, class Allocator >
void narytree< T, Allocator >::copy_node(
	const_iterator b, 
	const_iterator e, 
	iterator to )
{
	for ( const_iterator i = b; i != e; i ++ )
	{
		iterator it = insert( to, *i );
		
		copy_node( i.begin(), i.end(), it.end() );
	}
}

////////////////////////////////////////////////////////////////////////////////
////
//// public methods
////

template < class T, class Allocator >
typename narytree< T, Allocator >::iterator narytree< T, Allocator >::insert(
	const iterator & it, 
	const T & t )
{
	node_base * n = create_node( t );

	n->prev = it._n->prev;
	
	n->next = it._n;
	
	n->parent = it._n->parent;
	
	it._n->prev = n;
	
	n->prev->next = n;

	node_base * i = it._n;
	
	// 遡って子の数の増加を通知
	while ( ( i = (node_base *)i->parent ) != 0 ) i->des_size ++;
	
	return iterator( n );
}

template < class T, class Allocator >
typename narytree< T, Allocator >::iterator narytree< T, Allocator >::erase( const iterator & it )
{
	node_base * ret = (node_base *)it._n->next;

	it._n->prev->next = it._n->next;
	
	it._n->next->prev = it._n->prev;
	
	size_type del_des = it._n->des_size + 1;
	
	delete_node_des( (node *)it._n );

	node_base * i = ret;
	
	// 遡って子の数の減少を通知
	while ( ( i = (node_base *)i->parent ) != 0 ) i->des_size -= del_des;
	
	return iterator( ret );
}

template < class T, class Allocator >
void narytree< T, Allocator >::clear()
{
	while ( begin() != end() ) erase( begin() );
}

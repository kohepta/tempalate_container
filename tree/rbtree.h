﻿/*!
 * \file	rbtree.h
 *
 * \brief	赤黒木テンプレート
 *
 * \note
 *
 */

#ifndef __RBTREE_H__
#define __RBTREE_H__

/*!
 * \class	rbtree
 *
 * \brief	赤黒木テンプレートクラス
 *
 * \tparam	T		値型
 */
template < class T >
class rbtree
{
public:
	/*！
	 * \fn			rbtree()
	 *
	 * \brief		デフォルトコンストラクタ
	 */
	rbtree();

	/*！
	 * \fn			~rbtree()
	 *
	 * \brief		デストラクタ
	 */
	virtual ~rbtree();

	/*！
	 * \fn			validate()
	 *
	 * \brief		ノード格納位置整合性チェック
	 */
	void validate();

	/*！
	 * \fn			find( const T & data ) const
	 *
	 * \brief		赤黒木ノード検索
	 *
	 * \tparam		T		値型
	 * \parma[in]	data	検索キーデータ
	 * \return		検索データポインタ
	 */
	T * find( const T & data ) const;

	/*！
	 * \fn			insert( const T & data ) const
	 *
	 * \brief		赤黒木ノード挿入
	 *
	 * \tparam		T		値型
	 * \parma[in]	data	挿入データ
	 * \return		挿入データポインタ
	 */
	T * insert( const T & data );

	/*！
	 * \fn			erase( const T & data ) const
	 *
	 * \brief		赤黒木ノード削除
	 *
	 * \tparam		T		値型
	 * \parma[in]	data	検索キーデータ
	 * \return		true	削除成功 
	 * \return		false	削除失敗
	 */
	bool erase( const T & data );

private:
	/*!
	 * \struct	Node
	 *
	 * \brief	赤黒木ノード構造体
	 */
	struct Node
	{
		Node * parent;			//!< 親ノード
		Node * left;			//!< 左子ノード
		Node * right;			//!< 右子ノード
		bool red;				//!< 赤色フラグ: true - 赤 false - 黒
		T data;					//!< ユーザーデータ
	};

private:
	/*！
	 * \fn			_validate( Node * node )
	 *
	 * \brief		ノード格納位置整合性チェック
	 *
	 * \param[in]	node	検査対象ノード
	 * \return		黒高さ
	 */
	UINT _validate( Node * node );

	/*！
	 * \fn			find( const T & data ) const
	 *
	 * \brief		赤黒木ノード検索
	 *
	 * \tparam		T		値型
	 * \parma[in]	data	検索キーデータ
	 * \return		検索ノード
	 */
	Node * _find( const T & data ) const;

	/*！
	 * \fn			_getRightMinimum( Node * node )
	 *
	 * \brief		右部分木最小ノード取得
	 *
	 * \parma[in]	node	右部分木ルートノード
	 * \return		右部分木最小ノード
	 */
	Node * _getRightMinimum( Node * node );

	/*！
	 * \fn			_rotateLeft( Node * node )
	 *
	 * \brief		左部分木回転
	 *
	 * \parma[in]	node	回転軸ノード
	 */
	void _rotateLeft( Node * node );

	/*！
	 * \fn			_rotateRight( Node * node )
	 *
	 * \brief		右部分木回転
	 *
	 * \parma[in]	node	回転軸ノード
	 */
	void _rotateRight( Node * node );

	/*！
	 * \fn			_repairWhenInserting( Node * node )
	 *
	 * \brief		ノード挿入時補正処理
	 *
	 * \parma[in]	node	基点ノード
	 */
	void _repairWhenInserting( Node * node );

	/*！
	 * \fn			_repairWhenInserting( Node * node )
	 *
	 * \brief		ノード削除時修正処理
	 *
	 * \parma[in]	node	基点ノード
	 */
	void _repairWhenErasing( Node * node );

private:
	Node * _root;			//!< ルートノード
	Node * _nil;			//!< 末端ノード
};

//! コンストラクタ
template < class T >
rbtree< T >::rbtree()
{
	_nil = NULL;
	_root = NULL;
}

//! デストラクタ
template < class T >
rbtree< T >::~rbtree()
{
	Node * p;

	for ( p = _root; p != _nil; )
	{
		if ( _nil == p->left && _nil == p->right )
		{
			Node * t;

			t = p->parent;

			if ( t->left == p )
			{
				t->left = _nil;
			}
			else
			{
				t->right = _nil;
			}

			delete p;

			p = t;
		}
		else if ( _nil != p->left )
		{
			p = p->left;
		}
		else
		{
			p = p->right;
		}
	}
	
	if ( NULL != _nil )
	{
		delete _nil;
	}
}

template < class T >
void rbtree< T >::validate()
{
	_validate( _root );
}

//! 探索
template < class T >
T * rbtree< T >::find( const T & data ) const
{
	T * t;
	Node * node;

	t = NULL;
	node = _find( data );

	if ( _nil != node )
	{
		t = &node->data;
	}

	return t;
}

//! 挿入
template < class T >
T * rbtree< T >::insert( const T & data )
{
	Node * p;
	Node * t;
	T * r;
	
	r = NULL;

	if ( NULL == _nil )
	{
		// 初回挿入時
		_nil = new Node();
		
		_nil->left = _nil;
		_nil->right =_nil;
		_nil->red = false;
		_root = _nil;
	}
	
	for ( t = _root, p = _nil; t != _nil && !( data == t->data ); )
	{
		p = t;

		if ( data < t->data )
		{
			t = t->left;
		}
		else
		{
			t = t->right;
		}
	}

	if ( t == _nil )
	{
		// node 新規作成
		Node * node;

		node = new  Node();

		node->data = data;
		node->parent = _nil;
		node->left = _nil;
		node->right = _nil;
		node->red = true;

		node->parent = p;

		if ( _nil == p )
		{
			_root = node;
		}
		else if ( node->data < p->data )
		{
			p->left = node;
		}
		else
		{
			p->right = node;
		}
		
		r = &node->data;
		
		_repairWhenInserting( node );
	}
	
	return r;
}

//! 削除
template < class T >
bool rbtree< T >::erase( const T & data )
{
	bool success = true;
	Node * node;
	Node * child;
	Node * target;

	node = _find( data );
	child = _nil;
	target = _nil;

	if ( _nil != node )
	{
		if ( _nil == node->left || _nil == node->right )
		{
			// 子が 1 つ以下
			target = node;
		}
		else
		{
			// 左右に子がいるので、右部分木の最小ノードを取得
			target = _getRightMinimum( node );
			node->data = target->data;
		}

		if ( _nil != target->left )
		{
			child = target->left;
		}
		else
		{
			child = target->right;
		}
		
		child->parent = target->parent;

		if ( _nil == target->parent )
		{
			_root = child;
		}
		else if ( target == target->parent->left )
		{
			target->parent->left = child;
		}
		else
		{
			target->parent->right = child;
		}

		if ( !target->red )
		{
			_repairWhenErasing( child );
		}

		if ( target == _root )
		{
			_root = _nil;
		}

		delete target;
	}
	else
	{
		success = false;
	}

	return success;
}

template < class T >
UINT rbtree< T >::_validate( Node * node )
{
	if ( _nil != node )
	{
		UINT a;
		UINT b;

		if ( node->red )
		{
			// 赤が赤を子に持つ
			_ASSERTLOG( 
				( !node->left->red && !node->right->red ),
				"!! red has red to child !!" );
		}

		a = _validate( node->left );
		b = _validate( node->right );

		// 左右部分木の黒高さが違う
		_ASSERTLOG( 
			( a == b ),
			"!! black height is different !!" );

		if ( !node->red )
		{
			// 黒高さをカウント
			a ++;
		}

		return a;
	}

	return 0;
}

template < class T >
typename rbtree< T >::Node * rbtree< T >::_find( const T & data ) const
{
	Node * node;

	for ( node = _root; _nil != node && !( data == node->data ); )
	{
		if ( data < node->data )
		{
			node = node->left;
		}
		else
		{
			node = node->right;
		}
	}

	return node;
}

template < class T >
typename rbtree< T >::Node * rbtree< T >::_getRightMinimum( Node * node )
{
	Node * p;

	p = node;

	if ( p->right != p->right->right )
	{
		for ( p = p->right; p->left != p->left->left; )
		{
			p = p->left;
		}
	}

	return p;
}

//! 左回転
template < class T >
void rbtree< T >::_rotateLeft( Node * node )
{
	Node * rnode;

	rnode = node->right;
	node->right = rnode->left;

	if ( _nil != rnode->left )
	{
		rnode->left->parent = node;
	}

	rnode->parent = node->parent;

	if ( _nil == node->parent )
	{
		_root = rnode;
	}
	else if ( node == node->parent->left )
	{
		node->parent->left = rnode;
	}
	else
	{
		node->parent->right = rnode;
	}

	rnode->left = node;
	node->parent = rnode;
	rnode->red = node->red;
	node->red = true;
}

//! 右回転
template < class T >
void rbtree< T >::_rotateRight( Node * node )
{
	Node * lnode;

	lnode = node->left;
	node->left = lnode->right;

	if ( _nil != lnode->right )
	{
		lnode->right->parent = node;
	}

	lnode->parent = node->parent;

	if ( _nil == node->parent )
	{
		_root = lnode;
	}
	else if ( node == node->parent->left )
	{
		node->parent->left = lnode;
	}
	else
	{
		node->parent->right = lnode;
	}

	lnode->right = node;
	node->parent = lnode;
	lnode->red = node->red;
	node->red = true;
}

//! 挿入時の木修正
template < class T >
void rbtree< T >::_repairWhenInserting( Node * node )
{
	Node * p;
	Node * uncle;

	for ( p = node; p->parent->red; )
	{
		if ( p->parent == p->parent->parent->left )
		{
			uncle = p->parent->parent->right;

			if ( uncle->red )
			{
				p->parent->red = false;
				uncle->red = false;
				p->parent->parent->red = true;
				p = p->parent->parent;
			}
			else
			{
				if ( p == p->parent->right )
				{
					p = p->parent;
					_rotateLeft( p );
				}
				
				_rotateRight( p->parent->parent );
			}
		}
		else
		{
			uncle = p->parent->parent->left;

			if ( uncle->red )
			{
				p->parent->red = false;
				uncle->red = false;
				p->parent->parent->red = true;
				p = p->parent->parent;
			}
			else
			{
				if ( p == p->parent->left )
				{
					p = p->parent;
					_rotateRight( p );
				}
				
				_rotateLeft( p->parent->parent );
			}
		}
	}

	_root->red = false;
}

template < class T >
void rbtree< T >::_repairWhenErasing( Node * node )
{
	Node * p;
	Node * sibling;

	for ( p = node; _root != p && !p->red; )
	{
		if ( p == p->parent->left )
		{
			sibling = p->parent->right;

			if ( sibling->red )
			{
				_rotateLeft( p->parent );
				sibling = p->parent->right;
			}

			if ( !sibling->left->red && !sibling->right->red )
			{
				sibling->red = true;
				p = p->parent;
			}
			else
			{
				if ( !sibling->right->red )
				{
					_rotateRight( sibling );
					sibling = p->parent->right;
				}
				
				_rotateLeft( p->parent );
				sibling->left->red = false;
				sibling->right->red = false;
				
				p = _root;
			}
		}
		else
		{
			sibling = p->parent->left;

			if ( sibling->red )
			{
				_rotateRight( p->parent );
				sibling = p->parent->left;
			}

			if ( !sibling->right->red && !sibling->left->red )
			{
				sibling->red = true;
				p = p->parent;
			}
			else
			{
				if ( !sibling->left->red )
				{
					_rotateLeft( sibling );
					sibling = p->parent->left;
				}
				
				_rotateRight( p->parent );
				sibling->left->red = false;
				sibling->right->red = false;
				
				p = _root;
			}
		}
	}

	p->red = false;
}

#endif

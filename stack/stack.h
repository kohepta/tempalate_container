﻿#pragma once

#include <memory>

template < typename T, class Allocator = std::allocator< T > >
class Stack
{
public:
	typedef T								value_type;
	typedef Allocator						allocator_type;
	typedef typename Allocator::size_type	size_type;

private:
	// リンク
	struct Link
	{
		Link * prev;
		Link * next;
	};

	// ヘッド
	struct Head
		: public Link
	{
		size_type size;
	};

	// ノード
	struct Node
		: public Link
	{
		value_type data;
	};

private:
	typedef typename Allocator::template rebind< Node >::other		rebind_type;

private:
	Node * create_node( const T & t )
	{
		Node * n = ralloc_.allocate( 1 );
	
		init_link( n );
	
		try
		{
			alloc_.construct( &( n->data ), t );
		}
		catch ( ... )
		{
			ralloc_.deallocate( n, 1 );
		}

		return n;
	}

	void delete_node( Node * n )
	{
		alloc_.destroy( &( n->data ) );

		ralloc_.deallocate( n, 1 );
	}

	void init_link( Link * i )
	{
		i->prev = i->next = i;
	}

	void init_head()
	{
		init_link( &head_ );

		head_.size = 0;
	}

	void delete_all_nodes()
	{
		for ( Link * i = head_.next; i != &head_; )
		{
			Node * r = reinterpret_cast< Node * >( i );
		
			i = i->next;
		
			delete_node( r );
		}
	}

private:
	Head head_;				// ヘッド・ノード
	rebind_type ralloc_;	// ノードアロケータ
	Allocator alloc_;		// アロケータ

public:
	Stack()
	{
		init_head();
	}

	~Stack()
	{
		delete_all_nodes();
	}

	Stack( const Stack< T, Allocator > & other )
	{
		init_head();
	
		for ( Link * i = other.head_.next; i != &other.head_; i = i->next )
		{
			Node * n = reinterpret_cast< Node * >( i );

			push( n->data );
		}
	}

	Stack & operator=( const Stack< T, Allocator > & other )
	{
		if ( this == &other )
		{
			return *this;
		}

		delete_all_nodes();
	
		for ( Link * i = other.head_.next; i != &other.head_; i = i->next )
		{
			Node * n = reinterpret_cast< Node * >( i );

			push( n->data );
		}

		return *this;
	}
	
	void pop()
	{
		if ( &head_ == head_.prev )
		{
			return;
		}
	
		Node * d = reinterpret_cast< Node * >( head_.prev );
	
		head_.prev = head_.prev->prev;
	
		head_.prev->next = &head_;
	
		head_.size --;
	
		delete_node( d );
	}

	void push( const T & t )
	{
		Node * n = create_node( t );

		n->next = &head_;

		n->prev = head_.prev;
	
		head_.prev->next = n;
	
		head_.prev = n;
	
		head_.size ++;
	}

	bool empty()
	{
		return &head_ == head_.prev;
	}

	size_type size()
	{
		return head_.size;
	}

	T & peek( size_t amount = 0 )
	{
		Link * link = head_.prev;

		for ( size_t i = 0; i < amount && i < size(); i ++ )
		{
			link = link->prev;
		}
		
		return reinterpret_cast< Node * >( link )->data;
	}
};
